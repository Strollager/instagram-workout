role :app, %w{deploy@5.101.115.11}
role :web, %w{deploy@5.101.115.11}
role :db,  %w{deploy@5.101.115.11}

server '5.101.115.11', user: 'deploy', roles: %w{web app}
set :deploy_to, '/var/www/codingtime'
