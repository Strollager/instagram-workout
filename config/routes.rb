Rails.application.routes.draw do

  root 'application#index'
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  resources :users do
    member do
      get 'recent_media'
    end
  end

  resources :media do
    collection do
      post 'sync'
    end
  end

end
