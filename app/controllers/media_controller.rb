class MediaController < ApplicationController

  before_filter :authenticate_user!

  def create
    obj = Media.build_from(current_user, media_params[:original_image_url])
    render :json => obj
  end

  def sync
    Media.sync(current_user)
    render :json => { :text => 'Sync complete.' }
  end

  private

    def media_params
      params.require(:media).permit(:original_image_url)
    end

end
