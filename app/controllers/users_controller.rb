class UsersController < ApplicationController

  before_filter :authenticate_user!

  def recent_media
    respond_to do |format|
      format.html { render :locals => { :media => current_user.medias } }
    end
  end

end
