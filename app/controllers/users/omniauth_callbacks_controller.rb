class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def instagram
    @user = User.find_for_instagram_oauth request.env["omniauth.auth"]
    if @user.persisted?
      sign_in_and_redirect @user, :event => :authentication
    else
      flash[:notice] = "authentication error"
      redirect_to root_path
    end
  end

end
