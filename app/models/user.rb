class User < ActiveRecord::Base
  devise :database_authenticatable, :omniauthable

  has_many :medias

  # --------------------------------------------------------------------------------
  # Поиск/регистрация пользователя
  # --------------------------------------------------------------------------------
  # @note
  #   Ищем пользователя в базе
  #   если пользователь отсутствует -> регистрируем
  # --------------------------------------------------------------------------------
  # @return [Object] текущий пользователь
  # --------------------------------------------------------------------------------
  def self.find_for_instagram_oauth access_token
    if user = User.find_by_nickname(access_token.extra.raw_info.username)
      user
    else
      user = User.new(:nickname => access_token.extra.raw_info.username,
                      :instagram_token => access_token.credentials.token,
                      :password => Devise.friendly_token[0,20])
      user.save(validate: false)
      user
    end
  end

end
