class Instagram
  require 'rest-client'

  def self.recent_media_for user
    request = RestClient.get("https://api.instagram.com/v1/users/self/media/recent",
                        :params => { :access_token => user.instagram_token })
    request.present? ? JSON.parse(request) : nil
  end

end
