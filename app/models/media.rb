class Media < ActiveRecord::Base
  require 'rest-client'

  belongs_to :user
  mount_uploader :server_image, ImageUploader

  def self.build_from current_user, image_url
    obj = Media.find_by :original_image_url => image_url
    if obj.present?
      obj
    else
      obj = Media.create :user_id => current_user.id, :original_image_url => image_url
      obj.remote_server_image_url = image_url
      obj.save
    end
  end

  def self.sync user
    media = Instagram.recent_media_for user
    media['data'].reverse_each do |item|
      self.build_from(user, item['images']['standard_resolution']['url'])
    end
  end
end
