class CreateMedia < ActiveRecord::Migration
  def change
    create_table :media do |t|
      t.belongs_to :user
      t.string :server_image
      t.string :original_image_url
    end
  end
end
