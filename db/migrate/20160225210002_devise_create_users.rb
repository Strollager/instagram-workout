class DeviseCreateUsers < ActiveRecord::Migration
  def change
    create_table(:users) do |t|

      t.string :nickname
      t.string :email
      t.string :encrypted_password
      t.string :instagram_token

      t.timestamps null: false
    end

    add_index :users, :instagram_token, unique: true
    add_index :users, :nickname,        unique: true
  end
end
